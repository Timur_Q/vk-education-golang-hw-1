package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

func itemsDiff(a []string, b []string) []string {
	m := make(map[string]byte)
	for _, k := range a {
		m[k] += 1
	}
	for _, k := range b {
		m[k] += 2
	}
	result := []string{}
	for k, v := range m {
		if v < 3 {
			result = append(result, k)
		}
	}
	sort.Slice(result, func(i, j int) bool {
		return result[i] < result[j]
	})
	return result
}

type Inventory struct {
	cells  [5]string
	filled int
}

func newInventory() *Inventory {
	return &Inventory{
		cells:  [5]string{},
		filled: 0,
	}
}

func (inv *Inventory) hasItem(item_name string) bool {
	for _, a := range inv.cells {
		if a == item_name {
			return true
		}
	}
	return false
}

func (inv *Inventory) addItem(item_name string) {
	inv.cells[inv.filled] = item_name
	inv.filled++
}

type Items struct {
	items []string
}

type Moves struct {
	moves []string
}

func newItems(items ...string) *Items {
	return &Items{
		items: items,
	}
}

func newMoves(rooms ...string) Moves {
	return Moves{
		moves: rooms,
	}
}

func (its Items) checkItem(item_name string) bool {
	for _, a := range its.items {
		if a == item_name {
			return true
		}
	}
	return false
}

func (ms Moves) checkMove(room string) bool {
	for _, a := range ms.moves {
		if a == room {
			return true
		}
	}
	return false
}

type Transaction struct {
	newRoom Room
	str     string
}

type State struct {
	cur_room    Room
	inventory   *Inventory
	door_locked bool
}

func newState() State {
	return State{
		cur_room:    newKitchen(),
		inventory:   newInventory(),
		door_locked: true,
	}
}

func (s *State) move(room string) string {
	t := s.cur_room.move(s, room)
	s.cur_room = t.newRoom
	return t.str
}

func (s *State) lookAround() string {
	return s.cur_room.lookAround(s)
}

func (s *State) useItem(item_name string, to string) string {
	if s.inventory.hasItem(item_name) {
		if item_name == "ключи" && to == "дверь" {
			s.door_locked = !s.door_locked
			if s.door_locked {
				return "дверь закрыта"
			} else {
				return "дверь открыта"
			}
		} else {
			return "не к чему применить"
		}
	} else {
		return "нет предмета в инвентаре - " + item_name
	}
}

func (s *State) takeItem(item_name string) string {
	if s.cur_room.checkItem(item_name) && !s.inventory.hasItem(item_name) {
		if s.inventory.hasItem("рюкзак") {
			s.inventory.addItem(item_name)
			return "предмет добавлен в инвентарь: " + item_name
		} else {
			return "некуда класть"
		}
	} else {
		return "нет такого"
	}
}

func (s *State) putOnBody(clothes string) string {
	if s.cur_room.checkItem(clothes) {
		s.inventory.addItem(clothes)
		return "вы надели: " + clothes
	} else {
		return "нет такого"
	}
}

var moves = map[string]func() Transaction{
	"кухня": func() Transaction {
		nK := newKitchen()
		str := "кухня, ничего интересного. можно пройти - " + strings.Join(nK.moves, ", ")
		return Transaction{nK, str}
	},
	"коридор": func() Transaction {
		nC := newCorridor()
		str := "ничего интересного. можно пройти - " + strings.Join(nC.moves, ", ")
		return Transaction{nC, str}
	},
	"комната": func() Transaction {
		nMr := newMyRoom()
		str := "ты в своей комнате. можно пройти - " + strings.Join(nMr.moves, ", ")
		return Transaction{nMr, str}
	},
	"улица": func() Transaction {
		nS := newStreet()
		str := "на улице весна. можно пройти - " + strings.Join(nS.moves, ", ")
		return Transaction{nS, str}
	},
}

type Room interface {
	move(state *State, room string) Transaction
	checkItem(item_name string) bool
	lookAround(state *State) string
}

type Kitchen struct {
	*Items
	Moves
}

func newKitchen() Kitchen {
	return Kitchen{
		Items: newItems("чай"),
		Moves: newMoves("коридор"),
	}
}

func (k Kitchen) lookAround(state *State) string {
	backPackInfo := "собрать рюкзак и "
	if state.inventory.hasItem("конспекты") {
		backPackInfo = ""
	}
	return "ты находишься на кухне, на столе: " +
		strings.Join(k.items, ", ") +
		", надо " + backPackInfo + "идти в универ. можно пройти - " +
		strings.Join(k.moves, ", ")
}

func (k Kitchen) move(state *State, room string) Transaction {
	if k.checkMove(room) {
		return moves[room]()
	} else {
		return Transaction{k, "нет пути в " + room}
	}
}

type MyRoom struct {
	*Items
	Moves
}

func newMyRoom() MyRoom {
	return MyRoom{
		Items: newItems("ключи", "конспекты", "рюкзак"),
		Moves: newMoves("коридор"),
	}
}

func (mr MyRoom) lookAround(state *State) string {
	backpackInfo := ", на стуле: рюкзак"
	boardInfo := "на столе: "
	itemsOnBoard := itemsDiff(mr.items, []string{"рюкзак"})
	if state.inventory.hasItem("рюкзак") {
		backpackInfo = ""
		itemsOnBoard = mr.items
	}
	if len(itemsDiff(itemsOnBoard, state.inventory.cells[:])) == 0 {
		boardInfo = "пустая комната"
	}
	return boardInfo + strings.Join(itemsDiff(itemsOnBoard, state.inventory.cells[:]), ", ") +
		backpackInfo + ". можно пройти - " + strings.Join(mr.moves, ", ")
}

func (mr MyRoom) move(state *State, room string) Transaction {
	if mr.checkMove(room) {
		return moves[room]()
	} else {
		return Transaction{mr, "нет пути в " + room}
	}
}

type Corridor struct {
	*Items
	Moves
}

func newCorridor() Corridor {
	return Corridor{
		Items: newItems(),
		Moves: newMoves("кухня", "комната", "улица"),
	}
}

func (c Corridor) lookAround(state *State) string {
	return "ничего интересного. можно пройти - " + strings.Join(c.moves, ", ")
}

func (c Corridor) move(state *State, room string) Transaction {
	if c.checkMove(room) {
		if room == "улица" && state.door_locked {
			return Transaction{c, "дверь закрыта"}
		}
		return moves[room]()
	} else {
		return Transaction{c, "нет пути в " + room}
	}
}

type Street struct {
	Moves
}

func newStreet() Street {
	return Street{
		Moves: newMoves("домой"),
	}
}

func (st Street) move(state *State, room string) Transaction {
	return Transaction{nil, ""}
}

func (st Street) lookAround(state *State) string {
	return "ты находишься на улице. можно пройти - " +
		strings.Join(st.moves, ", ")
}

func (st Street) checkItem(item_name string) bool {
	return false
}

func move(state *State, params ...string) string {
	room := params[0]
	return state.move(room)
}

func lookAround(state *State, params ...string) string {
	return state.lookAround()
}

func useItem(state *State, params ...string) string {
	item_name := params[0]
	to := params[1]
	return state.useItem(item_name, to)
}

func takeItem(state *State, params ...string) string {
	item_name := params[0]
	return state.takeItem(item_name)
}

func putOnBody(state *State, params ...string) string {
	item_name := params[0]
	return state.putOnBody(item_name)
}

var commands = map[string]func(a *State, params ...string) string{
	"осмотреться": func(a *State, params ...string) string { return lookAround(a, params...) },
	"идти":        func(a *State, params ...string) string { return move(a, params...) },
	"надеть":      func(a *State, params ...string) string { return putOnBody(a, params...) },
	"взять":       func(a *State, params ...string) string { return takeItem(a, params...) },
	"применить":   func(a *State, params ...string) string { return useItem(a, params...) },
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	state := newState()
	for true {
		fmt.Print("Enter text: ")
		input, _ := reader.ReadString('\n')
		str := state.changeByCommand(input)
		fmt.Println(str)
	}
}

var globalState State

func initGame() {
	/*
		эта функция инициализирует игровой мир - все команты
		если что-то было - оно корректно перезатирается
	*/
	globalState = newState()
}

func (s *State) changeByCommand(input string) string {
	argv := strings.Split(input+" ", " ")
	params := argv[1:]
	command := argv[0]
	if function, ok := commands[command]; ok {
		return function(s, params...)
	} else {
		return "неизвестная команда"
	}
}

func handleCommand(input string) string {
	return globalState.changeByCommand(input)
}
